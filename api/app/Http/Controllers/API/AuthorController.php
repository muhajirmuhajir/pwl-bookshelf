<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Author;
use App\Helpers\ApiHelper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthorController extends Controller
{
    public function create(Request $request)
    {
        try {
            $fields = $request->validate([
                'name' => 'required|string',
                'about' => 'required|string',
                'instagram' => 'string',
                'url_image' => 'string',
            ]);

            $author = Author::create($fields);
            return ApiHelper::success($author);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function read($id)
    {
        try {
            $author = Author::findOrFail($id);
            return ApiHelper::success($author);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function readAll()
    {
        $authors = Author::all();
        return ApiHelper::success($authors);
    }

    public function update(Request $request, $id)
    {
        try {
            $fields = $request->validate([
                'name' => 'required|string',
                'about' => 'required|string',
                'instagram' => 'string',
                'url_image' => 'string',
            ]);

            $author = Author::findOrFail($id);
            $author->update($fields);

            return ApiHelper::success($author);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function delete($id)
    {
        try {
            $author = Author::destroy($id);
            return ApiHelper::success($author);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }

    public function readBooks($id)
    {
        try {
            $author = Author::query()
                ->with('books')
                ->findOrFail($id);
            return ApiHelper::success($author);
        } catch (Exception $error) {
            return ApiHelper::error($error);
        }
    }
}
