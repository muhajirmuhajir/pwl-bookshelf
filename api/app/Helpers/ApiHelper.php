<?php

namespace App\Helpers;

use Exception;

class ApiHelper
{
    public static function success($data)
    {
        return response(
            [
                'status' => 'success',
                'data' => $data
            ],
            200
        );
    }

    public static function error(Exception $error)
    {
        return response(
            [
                'status' => 'error',
                'data' => $error->getMessage()
            ],
            400
        );
    }
}
