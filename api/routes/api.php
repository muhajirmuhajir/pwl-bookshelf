<?php

use App\Http\Controllers\API\BookController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\AuthorController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('books')->group(function(){
    Route::post('/', [BookController::class, 'create']);
    Route::get('/{id}', [BookController::class, 'read']);
    Route::get('/', [BookController::class, 'readAll']);
    Route::put('/{id}', [BookController::class, 'update']);
    Route::delete('/{id}', [BookController::class, 'delete']);
});

Route::prefix('categories')->group(function(){
    Route::post('/', [CategoryController::class, 'create']);
    Route::get('/{id}', [CategoryController::class, 'read']);
    Route::get('/', [CategoryController::class, 'readAll']);
    Route::put('/{id}', [CategoryController::class, 'update']);
    Route::delete('/{id}', [CategoryController::class, 'delete']);

    Route::get('/{id}/books', [CategoryController::class, 'readBooks']);
});

Route::prefix('authors')->group(function(){
    Route::post('/', [AuthorController::class, 'create']);
    Route::get('/{id}', [AuthorController::class, 'read']);
    Route::get('/', [AuthorController::class, 'readAll']);
    Route::put('/{id}', [AuthorController::class, 'update']);
    Route::delete('/{id}', [AuthorController::class, 'delete']);

    Route::get('/{id}/books', [AuthorController::class, 'readBooks']);
});

